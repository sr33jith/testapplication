<?php
/**
 * User: sr33jith
 * Date: 3/4/16
 * Time: 10:58 AM
 */
// Make sure you have Composer's autoload file included
require 'vendor/autoload.php';

GLOBAL $config;
// Create a connection, once only.
$config = array(
    'driver' => 'mysql', // Db driver
    'host' => 'localhost',
    'database' => 'frog',
    'username' => 'root',
    'password' => '',
    'charset' => 'utf8', // Optional
    'collation' => 'utf8_unicode_ci', // Optional
    'prefix' => '', // Table prefix, optional
    'options' => array( // PDO constructor options, optional
        PDO::ATTR_TIMEOUT => 5,
        PDO::ATTR_EMULATE_PREPARES => false,
    ),
);
