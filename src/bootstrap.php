<?php
/**
 * Bootstrap file for loading config and settings
 * also setting the absolute path
 * User: sr33jith
 * Date: 31/3/16
 * Time: 2:02 PM
 */

// Define the PATH (also known absolute path)
define('PATH', dirname(__FILE__) . '/');

// Define the HELPER Path
define('HELPER', 'helpers/');

// Define the Model Path
define('MODELS', 'models/');

//Config Location
define("CONFIG", PATH . "config.php");

require_once(CONFIG);
