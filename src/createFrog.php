<?php
require_once('bootstrap.php');
require_once('header.php');
require_once(HELPER . 'FrogHelper.php');
require_once(HELPER . 'PondHelper.php');
$pondHelper = new PondHelper();
$ponds = $pondHelper::getAllPonds();
if (isset($_POST['createFrog'])) {
    $frogHelper = new FrogHelper();
    $created = $frogHelper->create();
    if ($created['status'] == 'success') {
        header("Location:frogs.php");
        die;
    } else {
        $errors = $created['data'];
    }
}
?>
    <div class="col-md-12 main">
        <h1 class="page-header">Create a Frog Entry</h1>

        <form method="post" action="">
            <div class="form-group">
                <label for="name">Name</label>
                <input type="text" class="form-control" id="name" name="name" placeholder="Name">
            </div>
            <div class="form-group row">
                <div class="col-xs-6">
                    <label for="capacity">Color</label>
                    <input type="text" class="form-control" id="color" name="color" placeholder="Color">
                </div>
                <div class="col-xs-6">
                    <label for="gender">Gender</label>
                    <select class="form-control" name="gender" id="gender">
                        <option value="male">Male</option>
                        <option value="female">Female</option>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-xs-6">
                    <label for="dob">Date Of Birth</label>
                    <input type="text" class="form-control" id="dob" name="dob" placeholder="Date Of Birth">
                </div>
                <div class="col-xs-6">
                    <label for="dod">Date Of Death</label>
                    <input type="text" class="form-control" id="dod" name="dod" placeholder="Date Of Death">
                </div>
            </div>
            <div class="form-group">
                <label for="pond">Select Pond</label><span class="right"> : No more vacant pond ? <a
                        href="createPond.php">Create new Pond</a></span>
                <select class="form-control" name="pond" id="pond">
                    <option value='0'>Select a pond</option>
                    <?php foreach ($ponds as $pond) {
                        $vacancy = ($pond->capacity - $pond->occupancy);
                        ?>
                        <option
                            value="<?php echo $pond->id ?>" <?php echo ($vacancy == 0) ? 'disabled' : ''; ?>><?php echo $pond->name ?>
                            - Vacant for - <?php echo $vacancy ?> Frogs
                        </option>
                    <?php } ?>
                </select>
            </div>
            <button type="submit" name="createFrog" class="btn btn-primary createFrog">Create</button>
        </form>
    </div>
<?php require_once('footer.php'); ?>