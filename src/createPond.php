<?php
require_once('bootstrap.php');
require_once('header.php');
require_once(HELPER . 'PondHelper.php');
if (isset($_POST['createPond'])) {
    $pondHelper = new PondHelper();
    $created = $pondHelper->create();
    if ($created['status'] == 'success') {
        header("Location:ponds.php");
        die;
    } else {
        $errors = $created['data'];
    }
}
?>
    <div class="col-md-12 main">
        <h1 class="page-header">Create a Pond</h1>

        <form method="post" action="">
            <div class="form-group">
                <label for="name">Name</label>
                <input type="text" class="form-control" id="name" name="name" placeholder="Name">
            </div>
            <div class="form-group row">
                <div class="col-xs-6">
                    <label for="environment">Environment</label>
                    <select class="form-control" name="environment" id="environment">
                        <option value="rainforest">Rain Forest</option>
                        <option value="montain">Montain</option>
                        <option value="desert">Desert</option>
                    </select>
                </div>
                <div class="col-xs-6">
                    <label for="capacity">Capacity</label>
                    <input type="number" class="form-control" id="capacity" name="capacity" placeholder="Capacity">
                </div>
            </div>
            <button type="submit" name="createPond" class="btn btn-primary createPond">Create</button>
        </form>
    </div>
    <script type="text/javascript">
        var capacity = document.getElementById("capacity");
        function validateCapacity() {
            if (!isEven(capacity.value)) {
                capacity.setCustomValidity("Only even values. e.g. 2,4,6 ...");
            }
        }
        function isEven(value) {
            if (value % 2 == 0)
                return true;
            else
                return false;
        }
        capacity.onkeyup = validateCapacity;
    </script>
<?php require_once('footer.php'); ?>