<?php
/**
 * User: sr33jith
 * Date: 3/4/16
 * Time: 3:07 PM
 */
?>
</div>
</div>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="js/jquery.min.js"><\/script>')</script>
<script src="js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script>
<!-- Just to make our placeholder images work. Don't actually copy the next line! -->
<script src="js/holder.min.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="js/ie10-viewport-bug-workaround.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('.deletePond').on('click', function () {
            var pondId = $(this).attr('data-text');
            $.ajax({
                method: "POST",
                url: "deletePond.php",
                data: { pondId: pondId }
            })
                .done(function (msg) {
                    location.reload();
                });
        });
        $('.deleteFrog').on('click', function () {
            var frogId = $(this).attr('data-text');
            $.ajax({
                method: "POST",
                url: "deleteFrog.php",
                data: { frogId: frogId }
            })
                .done(function (msg) {
                    location.reload();
                });
        });
        $('#dob')
            .datepicker({
                format: 'yyyy-mm-dd'
            });
        $('#dod')
            .datepicker({
                format: 'yyyy-mm-dd'
            });
    });
</script>
</body>
</html>