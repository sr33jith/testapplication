<?php
/**
 * User: sr33jith
 * Date: 3/4/16
 * Time: 8:59 PM
 */
require_once('bootstrap.php');
session_start();
if (!isset($_SESSION['user'])) {
    header("Location:signin.php");
    die;
}
$user = $_SESSION['user'];
require_once(HELPER . 'FrogHelper.php');
$frogHelper = new FrogHelper();
$frogId = (int)$_POST['frogId'];
$frogHelper->delete($frogId, $user->id);