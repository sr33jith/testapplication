/*
SQLyog Community v11.01 (32 bit)
MySQL - 5.5.38-0ubuntu0.14.04.1 : Database - frog
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`frog` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `frog`;

/*Table structure for table `frogs` */

DROP TABLE IF EXISTS `frogs`;

CREATE TABLE `frogs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `color` varchar(10) DEFAULT NULL,
  `gender` enum('male','female') NOT NULL DEFAULT 'male',
  `dob` date DEFAULT NULL,
  `dod` date DEFAULT NULL,
  `userId` int(11) NOT NULL,
  `pondId` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `CREATED_FROG_USER` (`userId`),
  KEY `FOR_PONDS` (`pondId`),
  CONSTRAINT `CREATED_FROG_USER` FOREIGN KEY (`userId`) REFERENCES `users` (`id`),
  CONSTRAINT `FOR_PONDS` FOREIGN KEY (`pondId`) REFERENCES `ponds` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

/*Data for the table `frogs` */

insert  into `frogs`(`id`,`name`,`color`,`gender`,`dob`,`dod`,`userId`,`pondId`,`created`) values (5,'frog3','white','male','2016-03-03','2016-04-03',16,1,'2016-04-03 23:17:42'),(7,'asdasa','black','female','2016-04-05','2016-04-14',14,1,'2016-04-04 21:44:05'),(8,'Frog3','RED','female','2016-04-01',NULL,14,1,'2016-04-04 22:26:14'),(9,'das','asd','male','2016-04-05',NULL,14,2,'2016-04-04 22:31:16'),(10,'Frog Test','White','male','2016-04-20',NULL,14,2,'2016-04-04 23:09:07');

/*Table structure for table `ponds` */

DROP TABLE IF EXISTS `ponds`;

CREATE TABLE `ponds` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `userId` int(11) NOT NULL,
  `capacity` int(11) NOT NULL DEFAULT '4',
  `environment` enum('rainforest','montain','desert') NOT NULL DEFAULT 'rainforest',
  `occupancy` int(11) DEFAULT '0',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `isDeleted` tinyint(4) DEFAULT '0',
  `status` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `CREATED_USER` (`userId`),
  CONSTRAINT `CREATED_USER` FOREIGN KEY (`userId`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `ponds` */

insert  into `ponds`(`id`,`name`,`userId`,`capacity`,`environment`,`occupancy`,`created`,`isDeleted`,`status`) values (1,'Ventive',16,12,'rainforest',2,'2016-04-03 20:22:43',0,1),(2,'Classical',14,14,'desert',2,'2016-04-04 22:25:46',0,1);

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `status` tinyint(4) DEFAULT '1',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

/*Data for the table `users` */

insert  into `users`(`id`,`email`,`password`,`status`,`created`) values (14,'sr33jith@gmail.com','d41d8cd98f00b204e9800998ecf8427e',1,'2016-04-03 16:18:15'),(15,'abin@xminds.com','d41d8cd98f00b204e9800998ecf8427e',1,'2016-04-03 17:19:33'),(16,'achu@gmail.com','d41d8cd98f00b204e9800998ecf8427e',1,'2016-04-03 20:19:30');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
