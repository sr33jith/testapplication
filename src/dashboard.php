<?php
require_once('bootstrap.php');
require_once('header.php');
require_once(HELPER . 'PondHelper.php');
require_once(HELPER . 'UserHelper.php');
$pondHelper = new PondHelper();
$userHelper = new UserHelper();
$ponds = $pondHelper::getAllPonds();
?>

    <div class="col-md-12 main">
        <h1 class="page-header">Dashboard</h1>
        <?php
        $i = 0;
        ?>
        <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

        <div class="row placeholders">
            <?php
            foreach ($ponds as $pond) {
                $maleCountRows = $pondHelper->getFrogCount($pond->id, 'male');
                $maleCount = count($maleCountRows);
                $femaleCountRows = $pondHelper->getFrogCount($pond->id, 'female');
                $femaleCount = count($femaleCountRows);
                if ($maleCount == 0 || $femaleCount == 0) {
                    echo "Pond " . $pond->name . " - no frog entries for the pond for generating chart.";
                    break;
                }
                $i++;
                ?>
                <div class="col-xs-6 col-sm-3 placeholder">
                    <div id="piechart<?php echo $i ?>" style="width: 500px; height: 200px;"></div>
                    <script type="text/javascript">
                        google.charts.load('current', {'packages': ['corechart']});
                        google.charts.setOnLoadCallback(drawChart);
                        function drawChart() {

                            var data = google.visualization.arrayToDataTable([
                                ['Task', 'Male Female ratio in the pond'],
                                ['Male', <?php echo $maleCount?>],
                                ['Female', <?php echo $femaleCount?>]
                            ]);

                            var options = {
                                title: 'Male Female ratio in <?php echo $pond->name?> Pond'
                            };

                            var chart = new google.visualization.PieChart(document.getElementById('piechart<?php echo $i?>'));

                            chart.draw(data, options);
                        }
                    </script>
                    <!--<h4>POND <?php /*echo $i;*/?></h4>-->
                        <span class="text-muted">Capacity : <?php echo $pond->capacity ?>,
                            Vacant for : <?php echo($pond->capacity - $pond->occupancy) ?></span>
                </div>
                <?php
                if ($i == 2) {
                    break;
                }
            }
            ?>
        </div>

        <!--<h2 class="sub-header">Section title</h2>
        <div class="table-responsive">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Header</th>
                    <th>Header</th>
                    <th>Header</th>
                    <th>Header</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>1,001</td>
                    <td>Lorem</td>
                    <td>ipsum</td>
                    <td>dolor</td>
                    <td>sit</td>
                </tr>
                <tr>
                    <td>1,002</td>
                    <td>amet</td>
                    <td>consectetur</td>
                    <td>adipiscing</td>
                    <td>elit</td>
                </tr>
                <tr>
                    <td>1,003</td>
                    <td>Integer</td>
                    <td>nec</td>
                    <td>odio</td>
                    <td>Praesent</td>
                </tr>
                <tr>
                    <td>1,003</td>
                    <td>libero</td>
                    <td>Sed</td>
                    <td>cursus</td>
                    <td>ante</td>
                </tr>
                <tr>
                    <td>1,004</td>
                    <td>dapibus</td>
                    <td>diam</td>
                    <td>Sed</td>
                    <td>nisi</td>
                </tr>
                <tr>
                    <td>1,005</td>
                    <td>Nulla</td>
                    <td>quis</td>
                    <td>sem</td>
                    <td>at</td>
                </tr>
                <tr>
                    <td>1,006</td>
                    <td>nibh</td>
                    <td>elementum</td>
                    <td>imperdiet</td>
                    <td>Duis</td>
                </tr>
                <tr>
                    <td>1,007</td>
                    <td>sagittis</td>
                    <td>ipsum</td>
                    <td>Praesent</td>
                    <td>mauris</td>
                </tr>
                <tr>
                    <td>1,008</td>
                    <td>Fusce</td>
                    <td>nec</td>
                    <td>tellus</td>
                    <td>sed</td>
                </tr>
                <tr>
                    <td>1,009</td>
                    <td>augue</td>
                    <td>semper</td>
                    <td>porta</td>
                    <td>Mauris</td>
                </tr>
                <tr>
                    <td>1,010</td>
                    <td>massa</td>
                    <td>Vestibulum</td>
                    <td>lacinia</td>
                    <td>arcu</td>
                </tr>
                <tr>
                    <td>1,011</td>
                    <td>eget</td>
                    <td>nulla</td>
                    <td>Class</td>
                    <td>aptent</td>
                </tr>
                <tr>
                    <td>1,012</td>
                    <td>taciti</td>
                    <td>sociosqu</td>
                    <td>ad</td>
                    <td>litora</td>
                </tr>
                <tr>
                    <td>1,013</td>
                    <td>torquent</td>
                    <td>per</td>
                    <td>conubia</td>
                    <td>nostra</td>
                </tr>
                <tr>
                    <td>1,014</td>
                    <td>per</td>
                    <td>inceptos</td>
                    <td>himenaeos</td>
                    <td>Curabitur</td>
                </tr>
                <tr>
                    <td>1,015</td>
                    <td>sodales</td>
                    <td>ligula</td>
                    <td>in</td>
                    <td>libero</td>
                </tr>
                </tbody>
            </table>
        </div>-->
    </div>


<?php require_once('footer.php'); ?>