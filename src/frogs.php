<?php
require_once('bootstrap.php');
require_once('header.php');
require_once(HELPER . 'FrogHelper.php');
require_once(HELPER . 'PondHelper.php');
$pondHelper = new PondHelper();
$frogHelper = new FrogHelper();
$frogs = $frogHelper::getAllMyFrogs();
?>

    <div class="col-md-12 main">
        <h1 class="page-header">Frogs</h1>

        <div><span class="right"><form method="post" action="createFrog.php">
                    <button class="btn btn-primary">Create a Frog Entry</button>
                </form></span></div>
        <div class="table-responsive">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Color</th>
                    <th>Gender</th>
                    <th>Date Of Birth</th>
                    <th>Date Of Death</th>
                    <th>Pond</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $i = 1;
                foreach ($frogs as $frog) {
                    ?>
                    <tr>
                        <td><?php echo $i; ?></td>
                        <td><?php echo $frog->name; ?></td>
                        <td><?php echo $frog->color; ?></td>
                        <td><?php echo $frog->gender; ?></td>
                        <td><?php echo $frog->dob; ?></td>
                        <td><?php echo $frog->dod; ?></td>
                        <td><?php echo $pondHelper->get($frog->pondId)->name; ?></td>
                        <td>
                            <button name="deleteFrog" type="button" class="btn btn-danger deleteFrog"
                                    data-text="<?php echo $frog->id; ?>">Delete
                            </button>
                        </td>
                    </tr>
                    <?php
                    $i++;
                }
                ?>

                </tbody>
            </table>
        </div>
    </div>

<?php require_once('footer.php'); ?>