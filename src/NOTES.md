
Features

1. Signin and signup facility with field validations and email exists checking.
2. Each user can create there own ponds.
3. Users can see others pond - ability to delete is restricted to owner of the pond.
4. Each pond has a capacity to limit the frog allocation - currently restricted to have even numbers during pond creation.
5. Users can create frog entries and assign them to the ponds which has vacant space. (which doesnt have a vacant space will be in the disabled state on the dropdown)
6. Delete option for users to delete frogs
7. Delete option for ponds - uses soft delete to avoid already assigned frogs within the deleted pond.
8. Dashboard provides the male:female ratio of the ponds.



Libraries used with the application

1. Query build - installed using composer - for better query management.
2. Bootstrap css and js for the template

Created : model and helper class for better communication and arranged/readable queries
Validation filters used on php side before any insertions to the table.

To setup and run the application

1. Create a db 'frog' and load the test dump from /db folder
2. install the query builder plugin using composer on the src directory.
3. (For better access) - point a domain to the index file on src folder.


Test user accounts
achu@gmail.com / 123456
sr33jith@gmail.com / 123456

Note : users can be created through signup page.

