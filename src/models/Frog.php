<?php
/**
 * User: sr33jith
 * Date: 3/4/16
 * Time: 9:59 PM
 */
require_once 'DBConnector.php';
/**
 * Class Frog
 */
class Frog extends DBConnector
{
    /**
     * @var @columns
     */
    protected $id;
    protected $name;
    protected $userId;
    protected $pondId;
    protected $color;
    protected $gender;
    protected $dob;
    protected $dod;
    protected $created;

    /**
     * @var string @table
     */
    public $_table = 'frogs';

    /**
     * Find all for the current user/ conditional user
     *
     * @param int $id
     * @return mixed
     */
    public function findAllMy($id = 0)
    {
        $query = QB::table($this->_table);
        if ($id != 0) {
            $query = QB::table($this->_table)->where('userId', $id);
        }
        return $query->get();
    }

    /**
     * Custom function to accept multiple conditions to get all records
     *
     * @param $conditions
     * @return mixed
     */
    public function findAllBy($conditions)
    {
        $query = QB::table($this->_table);

        foreach ($conditions as $conditionKey => $conditionVal) {
            //echo $conditionKey.'_'.$conditionVal;die;
            $query->where($conditionKey, '=', $conditionVal);
        }
        return $query->get();
    }

}
