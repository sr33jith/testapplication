<?php
/**
 * User: sr33jith
 * Date: 3/4/16
 * Time: 11:23 AM
 */
/**
 * Class DBConnector
 */
class DBConnector
{
    /**
     * Constructor to initiate db connection using query builder
     */
    public function __construct()
    {
        global $config;
        new \Pixie\Connection('mysql', $config, 'QB');
    }

    /**
     * Find a record using id - primary key
     * @param $id
     * @return mixed
     */
    public function find($id)
    {
        return QB::table($this->_table)->find($id);
    }

    /**
     * Create a record
     * @param $userData
     * @return string
     */
    public function create($userData)
    {
        foreach ($userData as $userCol => $userColVal) {
            if (!property_exists(get_class($this), $userCol)) {
                return 'Invalid column';
            }
            $this->$userCol = $userColVal;
        }

        $insertId = QB::table($this->_table)->insert($userData);
        return $insertId;
    }

    /**
     * Find all records irrespective of any condition
     * @return mixed
     */
    public function findAll()
    {
        $query = QB::table($this->_table);
        return $query->get();
    }

    /**
     * Update a record based on the conditions passed in
     * @param array $where
     * @param $data
     */
    public function update($where = array(), $data)
    {
        $queryBuilder = QB::table($this->_table);
        foreach ($where as $whereCondKey => $whereCondVal) {
            $queryBuilder->where($whereCondKey, $whereCondVal);
        }
        $queryBuilder->update($data);
    }

    /**
     * Delete a record by record id
     * @param $id
     */
    public function delete($id)
    {
        $queryBuilder = QB::table($this->_table);
        $queryBuilder->where('id', '=', $id);
        $queryBuilder->delete();
    }
}
