<?php
/**
 * User: sr33jith
 * Date: 3/4/16
 * Time: 11:11 AM
 */
require_once 'DBConnector.php';
/**
 * Class User
 */
class User extends DBConnector
{
    /**
     * @var @columns
     */
    protected $id;
    protected $email;
    protected $password;
    protected $status;
    protected $created;

    /**
     * @var string @table
     */
    public $_table = 'users';

}
