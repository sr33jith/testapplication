<?php
/**
 * User: sr33jith
 * Date: 3/4/16
 * Time: 4:59 PM
 */
require_once 'DBConnector.php';
/**
 * Class Pond
 */
class Pond extends DBConnector
{
    /**
     * @var @columns
     */
    protected $id;
    protected $name;
    protected $userId;
    protected $capacity;
    protected $environment;
    protected $occupancy;
    protected $status;
    protected $isDeleted;
    protected $created;

    /**
     * @var string @table
     */
    public $_table = 'ponds';

    /**
     * Function overriding parent function
     *
     * @param $isDeleted
     * @return mixed
     */
    public function findAll($isDeleted)
    {
        $query = QB::table($this->_table)->where('isDeleted', $isDeleted);
        return $query->get();
    }

}
