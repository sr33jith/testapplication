<?php
/**
 * User: sr33jith
 * Date: 3/4/16
 * Time: 8:59 PM
 */
require_once('bootstrap.php');
session_start();
if (!isset($_SESSION['user'])) {
    header("Location:signin.php");
    die;
}
$user = $_SESSION['user'];
require_once(HELPER . 'PondHelper.php');
$pondHelper = new PondHelper();
$pondId = (int)$_POST['pondId'];
$pondHelper->delete($pondId, $user->id);