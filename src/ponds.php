<?php
require_once('bootstrap.php');
require_once('header.php');
require_once(HELPER . 'PondHelper.php');
require_once(HELPER . 'UserHelper.php');
$pondHelper = new PondHelper();
$userHelper = new UserHelper();
$ponds = $pondHelper::getAllPonds();
?>

    <div class="col-md-12 main">
        <h1 class="page-header">Ponds</h1>

        <div><span class="right"><form method="post" action="createPond.php">
                    <button class="btn btn-primary">Create new Pond</button>
                </form></span></div>
        <div class="table-responsive">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Capacity</th>
                    <th>Occupancy</th>
                    <th>Environment</th>
                    <th>Owner</th>
                    <th>Delete</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $i = 1;
                foreach ($ponds as $pond) {
                    ?>
                    <tr>
                        <td><?php echo $i; ?></td>
                        <td><?php echo $pond->name; ?></td>
                        <td><?php echo $pond->capacity; ?></td>
                        <td><?php echo $pond->occupancy; ?> Frogs</td>
                        <td><?php echo $pond->environment; ?></td>
                        <td><?php echo $userHelper->get($pond->userId)->email; ?></td>
                        <td>
                            <button name="deletePond" type="button" class="btn btn-danger deletePond"
                                    data-text="<?php echo $pond->id; ?>">Delete
                            </button>
                        </td>
                    </tr>
                    <?php
                    $i++;
                }
                ?>

                </tbody>
            </table>
        </div>
    </div>

<?php require_once('footer.php'); ?>