<?php
/**
 * User: sr33jith
 * Date: 3/4/16
 * Time: 3:10 PM
 */
session_start();
unset($_SESSION['user']);
session_destroy();
header('Location:index.php');