<?php
/**
 * User: sr33jith
 * Date: 3/4/16
 * Time: 8:54 PM
 */
require_once 'models/Frog.php';
require_once 'models/User.php';
require_once 'models/Pond.php';

/**
 * Class FrogHelper
 */
class FrogHelper
{
    /**
     * Get Frog object
     * @param int $id
     * @return mixed
     */
    public static function get($id = 1)
    {
        $frogModel = new Frog();
        $frog = $frogModel->find($id);
        return $frog;
    }

    /**
     * Get all frogs
     * @return null
     */
    public static function getAllFrogs()
    {
        $frogModel = new Frog();
        $frogs = $frogModel->findAll();
        if ($frogs) {
            return $frogs;
        }
        return null;
    }

    /**
     * Get all frogs created by me
     * @return null
     */
    public static function getAllMyFrogs()
    {
        $frogModel = new Frog();
        $user = $_SESSION['user'];
        $frogs = $frogModel->findAllMy($user->id);
        if ($frogs) {
            return $frogs;
        }
        return null;
    }

    /**
     * Create a frog entry
     * @return array
     */
    public function create()
    {
        $frogModel = new Frog();
        $data = [];

        if (!filter_var($_POST['name'], FILTER_SANITIZE_SPECIAL_CHARS) === false) {
            $data['name'] = $_POST['name'];
        }
        if (!filter_var($_POST['color'], FILTER_SANITIZE_SPECIAL_CHARS) === false) {
            $data['color'] = $_POST['color'];
        }
        if (!filter_var($_POST['gender'], FILTER_SANITIZE_SPECIAL_CHARS) === false) {
            $data['gender'] = $_POST['gender'];
        }
        if (!$this->validateDate($_POST['dob']) === false) {
            $data['dob'] = $_POST['dob'];
        }
        if (!$this->validateDate($_POST['dod']) === false) {
            $data['dod'] = $_POST['dod'];
        }
        if (!filter_var($_POST['pond'], FILTER_VALIDATE_INT) === false) {
            $data['pondId'] = $_POST['pond'];
        }
        $data['userId'] = $_SESSION['user']->id;

        $pondHelper = new PondHelper();
        $pond = $pondHelper->get($data['pondId']);
        if (!$pond) {
            return ['status' => 'failed', 'data' => ['Invalid Pond']];
        }
        $frogId = $frogModel->create($data);

        // Update occupancy on the pond record
        $pondModel = new Pond();
        $where = ['id' => $pond->id];
        $data = ['occupancy' => ($pond->occupancy + 1)];
        $pondModel->update($where, $data);

        return ['status' => 'success', 'data' => $frogId];
    }

    /**
     * @param $date
     * @param string $format
     * @return bool
     */
    public function validateDate($date, $format = 'Y-m-d')
    {
        $d = DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) == $date;
    }

    /**
     * Delete a frog
     * @param $frogId
     * @param $userId
     */
    public function delete($frogId, $userId)
    {
        if ($frogId == 0 || $userId == 0) {
            $response = ['status' => 'failed', 'msg' => "Invalid inputs."];
            echo json_encode($response);
            die;
        }
        $frog = $this->get($frogId);
        if (!$frog) {
            $response = ['status' => 'failed', 'msg' => "Frog not found."];
            echo json_encode($response);
            die;
        }
        // check if the owner is deleting the frog entry
        if ($frog->userId != $userId) {
            $response = ['status' => 'failed', 'msg' => "Only frog owners can delete the frog."];
            echo json_encode($response);
            die;
        }

        // Reset frog entries
        $frogModel = new Frog();
        $frogModel->delete($frogId);
    }

}
