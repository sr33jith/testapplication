<?php
/**
 * User: sr33jith
 * Date: 3/4/16
 * Time: 4:54 PM
 */
require_once 'models/Pond.php';
require_once 'models/User.php';
require_once 'models/Frog.php';
/**
 * Class PondHelper
 */
class PondHelper
{
    /**
     * Get Pond record
     * @param int $id
     * @return mixed
     */
    public static function get($id = 1)
    {
        $pondModel = new Pond();
        $pond = $pondModel->find($id);
        return $pond;
    }

    /**
     * List all ponds
     * @param int $deleted
     * @return null
     */
    public static function getAllPonds($deleted = 0)
    {
        $pondModel = new Pond();
        $ponds = $pondModel->findAll($deleted);
        if ($ponds) {
            return $ponds;
        }
        return null;
    }

    /**
     * Create a pond
     * @return array
     */
    public function create()
    {
        $pondModel = new Pond();
        $data = [];
        if (!filter_var($_POST['capacity'], FILTER_VALIDATE_INT) === false) {
            $data['capacity'] = $_POST['capacity'];
        }
        if (!filter_var($_POST['name'], FILTER_SANITIZE_SPECIAL_CHARS) === false) {
            $data['name'] = $_POST['name'];
        }
        if (!filter_var($_POST['environment'], FILTER_SANITIZE_SPECIAL_CHARS) === false) {
            $data['environment'] = $_POST['environment'];
        }
        $data['userId'] = $_SESSION['user']->id;
        $pondId = $pondModel->create($data);
        return ['status' => 'success', 'data' => $pondId];
    }

    /**
     * Delete a pond
     *
     * uses soft delete to avoid miss placement of frogs within the pond
     *
     * @param $pondId
     * @param $userId
     */
    public function delete($pondId, $userId)
    {
        if ($pondId == 0 || $userId == 0) {
            $response = ['status' => 'failed', 'msg' => "Invalid inputs."];
            echo json_encode($response);
            die;
        }
        $pond = $this->get($pondId);
        if (!$pond) {
            $response = ['status' => 'failed', 'msg' => "Pond not found."];
            echo json_encode($response);
            die;
        }
        // Only pond owners can delete the pond
        if ($pond->userId != $userId) {
            $response = ['status' => 'failed', 'msg' => "Only pond owners can delete the pond."];
            echo json_encode($response);
            die;
        }

        // Reset frog entries
        $pondModel = new Pond();
        $where = ['id' => $pondId];
        $data = ['isDeleted' => 1];
        $pondModel->update($where, $data);
    }

    /**
     * Get frog count
     *
     * based on gender if specified
     *
     * @param $pondId
     * @param string $gender none, male, female
     * @return mixed
     */
    public function getFrogCount($pondId, $gender = 'none')
    {
        $conditions = [];
        if ($pondId != 0) {
            $conditions['pondId'] = $pondId;
        }

        if ($gender != 'none') {
            $conditions['gender'] = $gender;
        }
        $frogModel = new Frog();
        return $frogModel->findAllBy($conditions);
    }

}
