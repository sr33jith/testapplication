<?php
/**
 * User: sr33jith
 * Date: 3/4/16
 * Time: 11:34 AM
 */

require_once 'models/User.php';
/**
 * Class UserHelper
 */
class UserHelper
{
    /**
     * Get user object
     * @param int $id
     * @return mixed
     */
    public static function get($id = 1)
    {
        $userModel = new User();
        $user = $userModel->find($id);
        return $user;
    }

    /**
     * Create a user - signup
     * @return array
     */
    public function create()
    {
        $data = [];
        if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) === false) {
            $data['email'] = $_POST['email'];
        } else {
            $error[] = $_POST['email'] . " is not a valid email address";
        }
        // check email exists
        if ($this->checkEmailExists($_POST['email'])) {
            $error[] = $_POST['email'] . " is already used by another user. Please try another.";
        }
        if (isset($error)) {
            return ['status' => 'failed', 'data' => $error];
        }

        // md5 encryption for passwords
        $data['password'] = md5(mysql_real_escape_string($_POST['password']));

        $userModel = new User();
        $userId = $userModel->create($data);
        if ($userId) {
            session_start();
            $_SESSION['user'] = $this->get($userId);
        }
        return ['status' => 'success', 'data' => $userId];
    }

    /**
     * Login function
     * @return array
     */
    public function login()
    {
        if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) === false) {
            $data['email'] = $_POST['email'];
        } else {
            $error[] = $_POST['email'] . " is not a valid email address";
        }
        if (isset($error)) {
            return ['status' => 'failed', 'data' => $error];
        }
        // encrypt the password with md5 and check against db
        $data['password'] = md5(mysql_real_escape_string($_POST['password']));
        $userModel = new User();
        $query = QB::table($userModel->_table)
            ->where('email', '=', $data['email'])
            ->where('password', '=', $data['password']);
        $user = $query->get();
        if ($user) {
            session_start();
            $_SESSION['user'] = $user[0];
            return ['status' => 'success', 'data' => $user[0]->id];
        } else {
            $error[] = "Invalid login credentials.";
            return ['status' => 'failed', 'data' => $error];
        }
    }

    /**
     * Check email address is already in use or not
     * @param $email
     * @return bool
     */
    public function checkEmailExists($email)
    {
        $userModel = new User();
        $query = QB::table($userModel->_table)->where('email', '=', $email);
        $user = $query->get();
        if ($user) {
            return true;
        }
        return false;
    }
}
